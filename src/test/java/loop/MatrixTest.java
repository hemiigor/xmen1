package loop;

import org.junit.Test;

import static loops.Matrix.multiplicar;

public class MatrixTest {
    @Test
    public void matrixMultiplication(){
        Double[][] first = {{4.00, 3.00, 5.67},
                            {2.00, 1.00, 5.67},
                           {5.43, 4.12, 5.67}};

        Double[][] second = {{-0.500, 1.500, 5.67},
                             {1.000, -2.0000, 5.67},
                               {3.23, 6.15, 5.67}};

        Double[][] result = multiplicar(first,second);

        for(int i =0; i < first.length; i++){
            for (int j =0; j <second.length; j++)
                System.out.print(result[i][j] +"");
            System.out.println();
        }
    }
}
