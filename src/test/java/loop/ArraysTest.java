package loop;

import org.junit.Test;

import static firstapp.FirstApp.println;
import static loops.ForLoopAndArray.equalityOfTwoArrays;
import static loops.ForLoopAndArray.uniqueArray;

public class ArraysTest {
@Test
public void checkingEqualityOfTwoArrays(){
    int[] array1 = null;
    int[] array2 = {2 , 5, 7, 8, 11};
    int[] array3 = {2,5,7,9,11};
    int[] array4 = {2,5,7,8,11};
    int[] array5 = null;
    int[] array6 = new int[1];
    int[] array7 = new int[1];

    if (equalityOfTwoArrays(array1, array2)) {
        System.out.println("Two arrays are equal.");
    }else{
        System.out.println("Two arrays are not equals");
    }
}

@Test
    public void checkingUniqueArray(){
    uniqueArray(new int[] {0, 3, -2, 4, 3, 2});

    println();
    println();

    uniqueArray(new int[] {10, 22, 10, 20, 11, 22});

    }
}
