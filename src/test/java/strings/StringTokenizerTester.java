package strings;

import org.junit.Test;
import user.User;

import static strings.StringTokenizerUtil.*;


public class StringTokenizerTester {
    @Test
    public void tokenize(){
        String str = "Hello world Ok!";
        String str1 = "Hello, world, Ok!";

        splitBySpace(str);
        splitByComma(str1);
    }


    @Test
    public void readCSV(){
        String path = "/home/igor/IdeaProjects/xmen1/src/main/resources/file.csv";
        ReadFile.readCsv(path);

    }

}
