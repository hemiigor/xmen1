package maps;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class HashMapExampleTest {

    @Test
    public void testMap() {

        Map<Integer, Integer> map = HashMapExample.frequency(100);

        for (Map.Entry entry : map.entrySet()) {
            assert entry.getValue() != null;
        }
    }
}
