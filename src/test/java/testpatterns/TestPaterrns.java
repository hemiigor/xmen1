package testpatterns;


import abstractFacrory.AbstractFactory;
import abstractFacrory.SpeciesFactory;
import builder.*;
import factory.Animal;
import factory.Dog;
import org.junit.Assert;
import org.junit.Test;
import patterns.SingletonExample;
import prototype.Person;

import java.util.Arrays;

public class TestPaterrns {

    @Test
    public  void  builser(){
        Student s = new Student.Builder().age(27).language(Arrays.asList("chinese","english"))
                .name("igor").build();
        System.out.println(s);

        Student student = new Student.Builder().age(45).build();
    }

    @Test
    public void singleton(){
        SingletonExample singletonExample = SingletonExample.getInstance();
        singletonExample.sayHello();

        SingletonExample singletonExample1 = SingletonExample.getInstance();
        singletonExample.sayHello();
    }

    @Test
    public void abstractFactory(){
        AbstractFactory abstractFactory = new AbstractFactory();

        SpeciesFactory speciesFactory1 = abstractFactory.getSpeciesFactory("reptile");

        Animal a1 = speciesFactory1.getAnimal("tyrannosaurus");
       // System.out.println("a1 sound : "  + a1.makeSound());
        assert a1.makeSound().equals("Roar");

        Animal a2 = speciesFactory1.getAnimal("snake");
      //  System.out.println("a2 sound : "  + a2.makeSound());
        assert a2.makeSound().equals("Hiss");

        SpeciesFactory speciesFactory2 = abstractFactory.getSpeciesFactory("mammal");

        Animal a3 = speciesFactory2.getAnimal("dog");
       // System.out.println("a3 sound : "  + a3.makeSound());
       // assert a3.makeSound().equals("Woof");
        Assert.assertEquals("Woof", a3.makeSound());

        Animal a4 = speciesFactory2.getAnimal("cat");
       // System.out.println("a4 sound : "  + a4.makeSound());
        //assert a4.makeSound().equals("Meow");
        Assert.assertEquals("Meow", a4.makeSound());

        Animal animal = new AbstractFactory().getSpeciesFactory("mammal").getAnimal("dog");
        //System.out.println(animal.makeSound());
        Assert.assertEquals("Woof", animal.makeSound());
    }

    @Test
    public void prototype(){
        Person person1 = new Person("Fred");
        System.out.println("person 1 " + person1);
        Person person2 = (Person) person1.doClone();
        System.out.println("person 2 " + person2);


    }

    @Test
    public void meaiofjg(){
    MealBuilder mealBuilder = new ItalienMealBuider();
        MealDirector mealDirector = new MealDirector(mealBuilder);
        mealDirector.constructMeal();
        Meal meal = mealDirector.getMeal();
        System.out.println("meal is  " + meal);

        mealBuilder = new JapaneseMealBuilder();
        mealDirector = new MealDirector(mealBuilder);
        mealDirector.constructMeal();
        meal = mealDirector.getMeal();
        System.out.println("meal is  " + meal);

        Meal x = new MealDirector(new JapaneseMealBuilder()).constructMeal().getMeal();
        System.out.println(x);

}}
