package testpatterns;


import org.junit.Test;

import java.io.UnsupportedEncodingException;

public class Bytes {

    @Test
            public void testBytes() throws UnsupportedEncodingException {
        String str = "Hello";
        String str1 = "Hello" + 0xA0;

        byte[] b1 = str1.getBytes();

        System.out.println(b1);

        byte[] b2 = str1.getBytes("UTF8");

        System.out.println(b2);
    }


}
