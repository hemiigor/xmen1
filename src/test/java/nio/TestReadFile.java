package nio;

import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestReadFile {

    final static String POM = "/home/igor/IdeaProjects/xmen1/pom.xml";

    @Test
    public void testReader(){


        //read file into strem, try-with-resources
        try(Stream<String> stream = Files.lines(Paths.get(POM))){
            stream.forEach(System.out::println);

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Test
    public void testSecondReader(){

        List<String> list = new ArrayList<>();

        try(Stream<String> stream = Files.lines(Paths.get(POM))){

//             1. filter line
//             2. convert all content to upper case
//             3. convert it into a List

            list = stream.filter(line -> line.startsWith("<"))
                    .map(String::toUpperCase)
                    .collect(Collectors.toList());

        }catch (IOException e){
            e.printStackTrace();
        }

        list.forEach(System.out::println);
    }

    @Test
    public void testReference(){

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(2);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);

        arrayList.forEach(TestReadFile::square);
        arrayList.forEach(new TestReadFile()::square2);
    }

    public static void square(int num){
        System.out.println(Math.pow(num,2));
    }

    public void square2(int num){
        System.out.println(Math.pow(num,3));
    }

    @Test
    public void paths() throws IOException{

        Path path = Paths.get(NioTest.PROJECT_PATH, "path.txt");
        String question = "To be or not to be?";
        Files.write(path,question.getBytes());


        Path path1 =  Paths.get(NioTest.PROJECT_PATH,"path.txt");
        System.out.println(path1.getFileName() + " " +
        path1.getName(0) + " " +
        path1.getNameCount() + " " +
        path1.subpath(0,2) +" " +
        path1.getParent() + " " +
        path1.getRoot());

        TestReadFile.convertPaths();
    }

    private static void convertPaths(){
        Path relative = Paths.get("src/main/resources/adidas.jpg");
        System.out.println("Relative path: " + relative);
        Path absolute = relative.toAbsolutePath();
        System.out.println("Absolute path: " + absolute);

    }

    @Test
    public void copy(){
        Path oldFile = Paths.get("/home/igor/IdeaProjects/xmen1/" , "path.txt");
        Path newFile = Paths.get("src/main/resources/","newFile.txt");

        try(OutputStream os = new FileOutputStream(newFile.toFile())){
            Files.copy(oldFile,os);
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
