package nio;

import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.HashSet;
import java.util.Set;

public class NioTest {

    final static String FIRST_FILE = "/home/igor/IdeaProjects/xmen1/newTest1.txt";
    final static String PROJECT_PATH = "/home/igor/IdeaProjects/xmen1/";

    @Test
    public void copy(){

        //Get the instance of Path of Source File
        Path pathSource = Paths.get(PROJECT_PATH,"paths.txt");

        //get the File to be written
        File file = new File(FIRST_FILE);

        try(FileOutputStream destination = new FileOutputStream(file);
            FileInputStream source = new FileInputStream(file)){
            //copy a file to destination file and returns no of bytes written
            long noOfBytes = Files.copy(pathSource, destination);
            System.out.println(noOfBytes);

            Path destPath1 = Paths.get(PROJECT_PATH, "newTest2.txt");

            noOfBytes = Files.copy(source,destPath1, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(noOfBytes);

            Path destPath2 = Paths.get(PROJECT_PATH, "newTest3.txt");

            Path target = Files.copy(destPath1,destPath2, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(target.getFileName());


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void  directory() throws IOException{
        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(perms);
        Path path = Paths.get(PROJECT_PATH,"Parent","Child","Test");

        Files.createDirectories(path,fileAttributes);

        //creates temp directory in the specified path
        Files.createTempDirectory(path,"Concretepage");
    }

    @Test
    public void file() throws IOException{

        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(perms);
        Path path = Paths.get("/home/igor/IdeaProjects/", "xmen1","file.txt");

        Files.createFile(path);

        Set<PosixFilePermission> posixFilePermissions = new HashSet<>();
        posixFilePermissions.add(PosixFilePermission.OWNER_READ);
        FileAttribute<Set<PosixFilePermission>> fileAttribute2 = PosixFilePermissions.asFileAttribute(posixFilePermissions);
        Path path2 = Paths.get("/home/igor/IdeaProjects/", "xmen1", "fileNew.txt");
        Files.createFile(path2, fileAttribute2);
    }
}
