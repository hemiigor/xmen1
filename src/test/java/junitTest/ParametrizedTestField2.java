package junitTest;

import junit.DataMethods;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static junit.framework.TestCase.assertEquals;

@RunWith(Parameterized.class)
public class ParametrizedTestField2 {

    private int m1;
    private int m2;

    public ParametrizedTestField2(int m1,int m2){
        this.m1=m1;
        this.m2 = m2;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        Object[][] data = new Object[][] {{1,2},{5,3},{121,4}};
        return Arrays.asList(data);

    }

    @Test
    public void testMultiPlyException(){
        DataMethods tester = new DataMethods();
        assertEquals("Result",m1 * m2,tester.multiply(m1,m2));
    }
}
