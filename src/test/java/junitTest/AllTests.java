package junitTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({DataMethodsTest.class,DataMethodTest2.class})
public class AllTests {
}
