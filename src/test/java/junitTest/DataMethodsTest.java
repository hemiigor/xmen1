package junitTest;

import junit.DataMethods;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DataMethodsTest {

    @Test
    public void testFindMax(){
        assertEquals(4, DataMethods.findMax(new int[]{2,4,1,5}));
        assertEquals(4, DataMethods.findMax(new int[]{-12,-1,-3,-4}));

        int test = DataMethods.findMax(new int[]{2,4,1,5});
        assert test == 5;
    }

    @Test
    public void multiplyOfZeroIntegersShouldReturnZero(){
        assertEquals("10 * 0 be 0", 0,DataMethods.multiply(10,0));
        assertEquals("0 * 10 be 0", 0,DataMethods.multiply(0,10));
        assertEquals("0 * 0 be 0", 0,DataMethods.multiply(0,0));
    }
}
