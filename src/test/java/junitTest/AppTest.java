package junitTest;

import junit.DataMethods;
import org.junit.jupiter.api.*;

import java.util.function.Supplier;


public class AppTest {

    @BeforeAll
     static void setup(){
        System.out.println("@BeforeAll execute");
    }

    @BeforeEach
     void setupThis(){
        System.out.println("@BeforeEach execute");
    }

    @Tag("DEV")
    @Test
     void testCalcOne(){
        System.out.println("=======Test One Executed========");
        Assertions.assertEquals(4, DataMethods.sum(2,2));
    }

    @Tag("PROD")
    @Disabled
    @Test
    void testCalcTwo(){
        System.out.println("========Test TWO Execute========");
        Assertions.assertEquals(6,DataMethods.sum(2,4));
    }

    @AfterEach
    void tearThis(){
        System.out.println("@AfterEach executed");
    }

    @AfterAll
    static void tear(){
        System.out.println("@Afterall executed");
    }

    @Tag("DEV")
    @Test
    void testCase(){
        //GenericSmpl will pass
        Assertions.assertNotEquals(3,DataMethods.sum(2,2));

        //Generic will fail
        Assertions.assertNotEquals(5,DataMethods.sum(2,2));

        //Generic will fail
        Supplier<String> messageSupplier = ()-> "DataMethodsTest.sum(2,2) test failed";
        Assertions.assertNotEquals(5,DataMethods.sum(2,2), messageSupplier);
        }
    }

