package junitTest;

import junit.DataMethods;
import org.junit.*;

import static org.junit.Assert.assertEquals;

public class DataMethodTest2 {

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        System.out.println("Before class");
    }

    @Before
    public void setUp() throws Exception{
        System.out.println("before");
    }

    @Test
    public void testFindMax() throws Exception{
        System.out.println("test case find max");
        assertEquals(4, DataMethods.findMax(new int[]{1,4,3,2}));
        assertEquals(-2,DataMethods.findMax(new int[]{-12,-5,-6,-2}));

    }

    @Test
    public void testCube(){
        System.out.println("test case cube");
        assertEquals(27,DataMethods.cube(3));
        }

    @Test
    public void testReverseWord(){
        System.out.println("test case reverse word");
        assertEquals("ym eman si nahk ",DataMethods.reverseWord("my name is khan "));
        }

    @After
    public void thearDown(){
        System.out.println("after");
    }

    @AfterClass
    public static void thearDownAfterClass(){
        System.out.println("after class");

    }

}
