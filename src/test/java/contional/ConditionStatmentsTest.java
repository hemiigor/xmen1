package contional;

import org.junit.Test;

import static conditional.ConditionalStatements.positiveOrNegative;

public class ConditionStatmentsTest {

    @Test
    public void posOrNegTest(){
        Integer x = -6;
        Integer y = 10;
        Integer q = 0;
        positiveOrNegative(x);
        positiveOrNegative(y);
        positiveOrNegative(q);
    }
}
