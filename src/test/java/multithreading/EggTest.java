package multithreading;

import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class EggTest extends Thread {

    @Test
    public void ChickenOrEgg() {

        Chicken chicken = new Chicken();
        Egg egg = new Egg();

        System.out.println("Who's first? Who will have the last word and won! ");

        for (int i = 0; i < (Math.random() + 5) * 2; i++) {
            if (i % 2 == 0) {
                chicken.start();
            } else {
                egg.start();
            }
        }
        System.out.println("We have a winner !");
    }

    @Test
    public void test2() {

        System.out.println("Who's first? Who will have the last word and won!");

        ExecutorService executor = Executors.newFixedThreadPool(2);
        Runnable task = () -> System.out.println("Chicken!");
        Runnable task1 = () -> System.out.println("Egg!");

        for (int i = 0; i < ((Math.random() * 2) + 1); i++) {

            if (i % 2 == 0) {

                Future<?> future = executor.submit(task);

            } else {

                Future<?> future1 = executor.submit(task1);
            }
        }
        executor.shutdown();
        System.out.println("We have a winner!");
    }
}