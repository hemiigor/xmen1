package polymophism;

import org.junit.Test;
import polymorphism.User;

import static junit.framework.Assert.assertEquals;


public class UserTest {

    @Test
    public void setPasswordNormal(){
        User u = new User();
        u.setPassword("qwerty");
        assertEquals("qwerty" , u.getPassword());
    }

    @Test
    public void setPasswordShort(){
        User u = new User();
        u.setPassword("aa");
        assertEquals(null , u.getPassword());
    }

    @Test
    public void setPasswordNull(){
        User u = new User();
        u.setPassword(null);
        assertEquals(null , u.getPassword());
    }

    @Test(expected = NullPointerException.class)
    public void setPasswordNull1(){
        User u = new User();
        u.setPassword(null);
        assertEquals(null , u.getPassword());
    }



    @Test
    public void setLoginIdValid() {
        User u = new User();
        u.setLoginId("a123");
        assertEquals("a123", u.getLoginId());
    }

    @Test
    public void setLoginIdInvalid() {
        User u = new User();
        u.setLoginId("@#$@$@");
        assertEquals(null, u.getLoginId());
    }
}
