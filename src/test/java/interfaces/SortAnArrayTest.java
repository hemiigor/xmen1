package interfaces;

import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortAnArrayTest {

    @Test
    public void sortAnArray(){
        String [] fruits = new String[] { "Pineapple" , "Apple", "Orange", "Banana"};

        Arrays.sort(fruits);

        int i = 0;
        for (String temp : fruits){
            System.out.println("fruits " + ++i +" : "+ temp);
        }
    }

    @Test
    public void sortAnArrayList(){
        List<String> fruits = new ArrayList<>();

        fruits.add("Pineapple");
        fruits.add("Apple");
        fruits.add("Banana");
        fruits.add("Orange");

        Collections.sort(fruits);

        int i=0;
        for(String temp : fruits){
            System.out.println(temp);
        }
    }


}
