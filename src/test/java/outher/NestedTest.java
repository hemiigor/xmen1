package outher;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.*;
import org.junit.runner.RunWith;

@RunWith(HierarchicalContextRunner.class)
public class NestedTest {

    @BeforeClass
    public static void beforeAllTestMethods() {
        System.out.println("Invoked once before all test methods");
    }

    @Before
    public  static void beforEachMethod(){
        System.out.println("Invoked before each test method");
    }

    @After
    public static void afterEachMethodTest(){
        System.out.println("Invoked after each test method");
    }

    @AfterClass
    public static void afterAllTestMethod(){
        System.out.println("Invoked once after all test methods");
    }

    @Test
    public void rootClassTest(){
        System.out.println("Root class Test");
    }

    public class ContextA{


        @Before
        public void beforEachMethodOfContextA(){
            System.out.println("Invoked before each test method of context A");
        }

        @After
        public void afterEachMethodOfContextA(){
            System.out.println("Invoked after each test method of context A");
        }

        @Test
        public void contextATest(){
            System.out.println("Context A test");
        }

        public class ContextC{

            @Before
            public void beforEachMethodOfContextC(){
                System.out.println("Invoked before each test method of context C");
            }

            @After
            public void afterEachMethodOfContextC(){
                System.out.println("Invoked after each test method of context C");
            }

            @Test
            public void contextCTest(){
                System.out.println("Context C test");
            }
        }


    }
}
