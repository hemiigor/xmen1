package collections;

import org.junit.Test;

import java.util.TreeSet;

public class MyCompUserDeline {

    @Test
    public void test(){

        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(6);
        treeSet.add(2);
        treeSet.add(4);

        treeSet.forEach(x -> System.out.println(x));


        TreeSet<Empl> nameComp = new TreeSet<>(new MyNameComp());
        nameComp.add(new Empl("Ram" ,3000 ));
        nameComp.add(new Empl("John" ,6000 ));
        nameComp.add(new Empl("Crish" ,2000 ));
        nameComp.add(new Empl("Tom" ,2400));

        for(Empl e : nameComp){
            System.out.println(e);
        }

        nameComp.forEach(e ->{
            System.out.println(e);
        });

        System.out.println("==============");

        TreeSet<Empl> salComp = new TreeSet<>(new MySalaryComp());
        salComp.add(new Empl("Ram" ,3000 ));
        salComp.add(new Empl("John" ,6000 ));
        salComp.add(new Empl("Crish" ,2000 ));
        salComp.add(new Empl("Tom" ,2400));

        for(Empl e : salComp){
            System.out.println(e);
        }
    }
}
