package test.firstapp;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static firstapp.FirstApp.print;
import static firstapp.FirstApp.println;


public class FirstAppTest {

    Integer x = 2018;

    /**
     * Testing our version for System.out.
     */
    @Test
    public void firstAppTest(){
        print("Hello World");
        println();
        println(x);

        List<Integer> integers = new ArrayList<>();
        integers.add(2);
        integers.add(10);
        integers.add(45);

        integers.forEach(integer -> System.out.println(integer));
        integers.forEach(System.out::println);



    }
}
