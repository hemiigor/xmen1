package exceptions;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.Rule;
import org.junit.Test;



import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.*;
import java.util.ArrayList;


import static junit.framework.TestCase.fail;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


@RunWith(HierarchicalContextRunner.class)
public class ExceptionsTest {

    @Test(expected = ArithmeticException.class)
    public void testDivisionExceptions() {
        int i = 1 / 0;
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyList() {
        new ArrayList<>().get(0);
    }

    public class ExceptionTest2 {

        @Test
        public void testDivisionWithException() {
            try {
                int i = 1 / 0;
                fail();
            } catch (ArithmeticException e) {
                assertThat(e.getMessage(), is("/ by zero"));
            }
        }

        @Test
        public void testFail() {

            try {
                CustomerService customerService = new CustomerService();
                customerService.findByName("");

                fail();
            } catch (Exception e) {

                System.out.println(e.getMessage());

            }
        }
    }

    public class Exceptions3{

        @Rule
        public ExpectedException thrown = ExpectedException.none();

        @Test
        public void testDivisionWithException(){

            thrown.expect(ArithmeticException.class);
            thrown.expectMessage(containsString("/ by zero"));

            int i = 1 / 0;

        }

        @Test
        public void testNameNotFoundException() throws  NameNotFoundException {

            thrown.expect(NameNotFoundException.class);

            thrown.expectMessage(is("Name is empty!"));

            thrown.expect(hasProperty("errCode"));
            thrown.expect(hasProperty("errCode",is(687)));

            CustomerService cost = new CustomerService();

            cost.findByName("");
        }

        @Test
        public void test(){

            try{
                InputStream inputStream = new FileInputStream(new File("jhgjhg"));
            }catch (FileNotFoundException e){
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                System.out.println(ExceptionUtils.getStackTrace(e));
            }
        }
    }
}
