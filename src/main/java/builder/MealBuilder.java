package builder;

public interface MealBuilder {
    void buildDrink();

    void buildMainCOurse();

    void buildSide();

    Meal getMeal();
}
