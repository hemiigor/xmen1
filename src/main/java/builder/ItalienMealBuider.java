package builder;

public class ItalienMealBuider implements  MealBuilder {

   private Meal meal;

   public ItalienMealBuider(){
       meal = new Meal();
   }
    @Override
    public void buildDrink() {
       meal.setDrink("red wine");
    }

    @Override
    public void buildMainCOurse() {
       meal.setMainCourse("pizza");

    }

    @Override
    public void buildSide() {
       meal.setMainCourse("bread");

    }

    @Override
    public Meal getMeal() {
        return meal;
    }
}
