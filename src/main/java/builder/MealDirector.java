package builder;

public class MealDirector implements MealBuilder {

    private MealBuilder mealBuilder = null;

    public MealDirector(MealBuilder mealBuilder){
        this.mealBuilder = mealBuilder;
    }

    public MealBuilder constructMeal() {
        mealBuilder.buildDrink();
        mealBuilder.buildMainCOurse();
        mealBuilder.buildSide();
        return this;
    }

    @Override
    public void buildDrink() {

    }

    @Override
    public void buildMainCOurse() {

    }

    @Override
    public void buildSide() {

    }

    public Meal getMeal(){
        return mealBuilder.getMeal();
    }
}
