package workers;

import interfaces.Bonus;
import interfaces.Overtime;
import interfaces.Rate;

import java.util.Comparator;

public class Pilot  implements Comparable<Pilot>, Rate, Bonus {

    private String workerName;
    private  final String workName = "Pilot";
    private int salary;
    private int id;

    Pilot(String workerName, int salary, int id) {
        this.workerName = workerName;
        this.salary = salary;
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    @Override
    public void payBonus() {

    }

    @Override
    public void payRate() {

    }

    @Override
    public int compareTo(Pilot o) {
        return this.id = o.id;
    }

    public static Comparator<Pilot> nameComparator = new Comparator<Pilot>() {
        @Override
        public int compare(Pilot o1, Pilot o2) {
            return o1.workName.compareTo(o2.workName);
        }
    };
}
