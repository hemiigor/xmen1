package workers;

import interfaces.Bonus;
import interfaces.Hourly;
import interfaces.Overtime;

public class Manager implements Comparable<Manager>, Bonus, Hourly, Overtime {

    private String workerName;
    private final String workName = "Manager";
    private int salary;
    private int id;

    Manager(String workerName,int salary, int id) {
        this.workerName = workerName;
        this.salary = salary;
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    @Override
    public void payBonus() {

    }

    @Override
    public void payHourly() {

    }

    @Override
    public int compareTo(Manager o) {
        return this.id = o.id;
    }

    @Override
    public void payOvertime() {

    }
}
