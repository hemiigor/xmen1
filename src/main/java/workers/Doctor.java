package workers;

import interfaces.Bonus;
import interfaces.Overtime;
import interfaces.Rate;

public class Doctor implements Comparable <Doctor>, Rate, Bonus, Overtime {

    private String workerName;
    private final String workName = "Doctor";
    private int salary;
    private int id;

    Doctor(String workerName, int salary, int id) {
        this.workerName = workerName;
        this.salary = salary;
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    @Override
    public void payBonus() {

    }

    @Override
    public void payRate() {

    }



    @Override
    public int compareTo(Doctor o) {
         return this.id = o.id;
    }

    @Override
    public void payOvertime() {

    }
}
