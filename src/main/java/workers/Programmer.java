package workers;

import interfaces.Bonus;
import interfaces.Hourly;
import interfaces.Overtime;
import interfaces.Rate;

public class Programmer implements Comparable<Programmer>, Rate, Hourly, Overtime, Bonus {

    private String workerName;
    private final String workName = "Programmer";
    private int salary;
    private int id;

    Programmer(String workerName, int salary, int id) {
        this.workerName = workerName;
        this.salary = salary;
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    @Override
    public void payBonus() {

    }

    @Override
    public void payHourly() {

    }

    @Override
    public void payOvertime() {

    }

    @Override
    public void payRate() {

    }

    @Override
    public int compareTo(Programmer o) {
        return this.id = o.id;
    }
}
