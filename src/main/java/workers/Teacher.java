package workers;

import interfaces.Hourly;
import interfaces.Overtime;
import interfaces.Rate;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Teacher  implements Comparable<Teacher> , Rate, Hourly, Overtime {

    private String workerName;
    private final String workName = "Teacher";
    private int salary;
    private  int id;

    Teacher(String workerName, int salary, int id) {
        this.workerName = workerName;
        this.salary = salary;
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    @Override
    public int compareTo(Teacher o) {
        return this.id = o.id;
    }

    @Override
    public void payRate() {
        
    }

    @Override
    public void payHourly() {

    }

    @Override
    public void payOvertime() {

    }





}
