package workers;

import interfaces.Hourly;
import interfaces.Tips;

public class Driver implements Comparable<Driver> , Hourly, Tips {

    private String workerName;
    private final String workName = "Driver";
    private int salary;
    private int id;

    Driver(String workerName, int salary, int id) {
        this.workerName = workerName;
        this.salary = salary;
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    @Override
    public void payHourly() {

    }

    @Override
    public void payTips() {

    }

    @Override
    public int compareTo(Driver o) {
        return this.id = o.id;
    }
}
