package workers;

import interfaces.Bonus;
import interfaces.Rate;

public class Boss implements Comparable<Boss>, Rate, Bonus {

    private String workerName;
    private final String workName = "Boss";
    private int salary;
    private int id;

    Boss(String workerName, int salary, int id) {
        this.workerName = workerName;
        this.salary = salary;
         this.id = id;
    }

    public String getWorkName() {
        return workName;
    }


    @Override
    public void payBonus() {
    salary = salary +1000;
    }

    @Override
    public void payRate() {
        salary = 3000;

    }

    @Override
    public int compareTo(Boss o) {
        return this.id = o.id ;
    }
}
