package workers;

import interfaces.Rate;
import interfaces.Tips;

import java.util.Comparator;

public class Waiter implements Comparable<Waiter>, Rate, Tips {

    private String workerName;
    private final String workName = "Waiter";
    private int salary;
    private int id;

    Waiter(String workerName, int id){
        this.workerName = workerName;
        this.id = id;
    }

    public String getWorkName() {
        return workName;
    }

    @Override
    public void payRate() {
        salary = 1000;
        payTips();
        }

    @Override
    public void payTips() {
        if(salary < 1000)
        salary = salary + (salary / 100 * 20);

    }

    @Override
    public int compareTo(Waiter o) {
        return this.id = o.id;
    }

    public static Comparator<Waiter> workComparator = new Comparator<Waiter>() {
        @Override
        public int compare(Waiter o1, Waiter o2) {
            return o1.workName.compareTo(o2.workName);
        }
    };
}
