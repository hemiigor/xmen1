package junit;

import java.util.StringTokenizer;

public class DataMethods {

    public static int sum(int x, int y){
        return  x+y;
    }

    public static int multiply(int x, int y){
    return  x *y;
    }

    public static int findMax(int arr[]){
        int max = arr[0];

        for (int i=0; i< arr.length; i++){
            if( max < arr[i])
                max = arr[i];
        }
        return  max;
    }

    public  static int cube(int n){
        return n * n * n;
    }

    public static String reverseWord(String str){
        StringBuilder result = new StringBuilder();
        StringTokenizer tokenize = new StringTokenizer(str, " ");

        while(tokenize.hasMoreTokens()){
            StringBuilder sb = new StringBuilder();
            sb.append(tokenize.nextToken());
            sb.reverse();

            result.append(sb);
            result.append(" ");

        }
        return  result.toString();

    }
}

