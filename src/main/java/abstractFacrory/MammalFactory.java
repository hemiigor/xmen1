package abstractFacrory;

import factory.Animal;
import factory.Cat;
import factory.Dog;

public class MammalFactory extends SpeciesFactory {

    @Override
    public Animal getAnimal(String type) {
        if ("dog".equals(type)) {
            return new Dog();
        }else{
            return  new Cat();

        }
    }
}
