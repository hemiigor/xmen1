package abstractFacrory;

import factory.Animal;

public abstract class SpeciesFactory {
    public  abstract Animal getAnimal(String type);
}
