package multithreading;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorsTest {

    public static void main(String...args){

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            String threadName = Thread.currentThread().getName();
            try {
                TimeUnit.SECONDS.sleep(4);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            System.out.println("Hello " + threadName);
        });

        try{
            System.out.println("attempt to shutdown executor");
            Thread.sleep(10000);
            executorService.shutdown();
            executorService.awaitTermination(5,TimeUnit.SECONDS);
        }catch (InterruptedException e){
            System.err.println("task interrupted");
        } finally {
            if(!executorService.isTerminated()){
                System.out.println("cancel non-finished tasks");
            }
            executorService.shutdown();
            System.out.println("shutdown finished");
        }
    }
}
