package multithreading;

import java.util.concurrent.atomic.AtomicInteger;

public class RunnableDemo implements Runnable {

    private Thread t;
    private volatile String threadName;
    static Integer x =0;
    static AtomicInteger y = new AtomicInteger(10);

    RunnableDemo(String name){
        threadName =name;
        System.out.println("Creating " + threadName);
    }

    @Override
    public void run() {
        System.out.println("Running " + threadName);
        try{
            for(int i = 4; i >0; i--){
                System.out.println("Thread " + threadName + ", " + i);
               //Let the thread sleep for a while
                Thread.sleep(2000);
            }
        }catch (InterruptedException e){
            System.out.println("Thread " + threadName + "interruped.");
        }
        System.out.println("Thread " + threadName + " exiting.");
        add();

    }

    public static void add(){

        synchronized (x){
            x++;
            System.out.println(x);
        }
        y.incrementAndGet();
        System.out.println(y);

    }

    public synchronized void start(){
        System.out.println("Starting " + threadName);

        if( t == null){
            t = new Thread(this, threadName);
            t.start();
        }
    }



}
