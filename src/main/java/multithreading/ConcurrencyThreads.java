package multithreading;

import java.util.concurrent.TimeUnit;

public class ConcurrencyThreads {

    public static void main(String...args){

        Runnable task = () -> {
            Thread.currentThread().setName("Vasya");
            String threadName = Thread.currentThread().getName();
            System.out.println(" Heloo "+ threadName);
        };

        task.run();
        Thread thread = new Thread(task);
        thread.start();

        System.out.println("Done!");

        Runnable runnable =() ->{
            try{
                String name = Thread.currentThread().getName();
                System.out.println("Foo " + name);
                TimeUnit.SECONDS.sleep(3);
                System.out.println("Bar " + name);
            }
            catch (InterruptedException e ){
                e.printStackTrace();
            }
        };

        Thread thread1 = new Thread(runnable);
        thread1.start();
    }
}
