package multithreading;

public class TestThread {

    public static  void main(String ... args){
        RunnableDemo r1 = new RunnableDemo("Thread 1 ");
        RunnableDemo r2 = new RunnableDemo("Thread 2 ");

        r1.start();
        r2.start();
    }
}
