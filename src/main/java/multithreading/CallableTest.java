package multithreading;

import sun.nio.ch.ThreadPool;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class CallableTest {

    public static void main(String...args) {
invokeAll1();
       invokeAll();
       System.out.println("=================================");
        invokeAll1();
        Callable<Integer> task = () -> {
            try {
                TimeUnit.SECONDS.sleep(1);
                return 123;
            } catch (InterruptedException e) {
                throw new IllegalStateException("task interrupted", e);
            }
        };
        try {
            Integer x = task.call();
            System.out.println(x);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Integer> future = executor.submit(task);

        System.out.println("future done " + future.isDone());

        Integer result = null;
        try {
            result = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
            executor.shutdownNow();
        }

        System.out.println("future done " + future.isDone());
        System.out.println("result" + result);
    }



      public static void invokeAll() {


          ExecutorService executor = Executors.newSingleThreadExecutor();

          List<Callable<String>> callables = Arrays.asList(
                  () -> "task1",
                  () -> "task2",
                  () -> "task3");
          try {
              executor.invokeAll(callables)
                      .stream()
                      .map(future -> {
                          try {
                              return future.get();
                          } catch (Exception e) {
                              throw new IllegalStateException(e);
                          }
                      })
                      .forEach(x -> {
                          try {
                              TimeUnit.SECONDS.sleep(2);
                              System.out.println(x);
                          } catch (InterruptedException e) {
                              e.printStackTrace();
                          }
                      });
          } catch (InterruptedException e) {
              e.printStackTrace();
          } finally {
              executor.shutdownNow();
          }

      }
          public static void invokeAll1 () {
              ExecutorService executor2 = Executors.newSingleThreadExecutor();

              List<Callable<String>> callables1 = Arrays.asList(
                      callable("task3 ", 2),
                      callable("task4 ", 1),
                      callable("task5 ", 3));


              try {
                  executor2.invokeAll(callables1)
                          .stream()
                          .map(future -> {
                              try {
                                  return future.get();
                              } catch (Exception e) {
                                  throw new IllegalStateException(e);
                              }
                          })
                          .forEach(x -> {
                              try {
                                  TimeUnit.SECONDS.sleep(2);
                                  System.out.println(x);
                              } catch (InterruptedException e) {
                                  e.printStackTrace();
                              }
                          });


              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
          }


      static Callable<String> callable(String result,long sleepSeconds){
          return() -> {
          TimeUnit.SECONDS.sleep(1);
          return result;
        };

    }
}
