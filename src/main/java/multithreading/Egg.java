package multithreading;

class Egg extends Thread {

    @Override
    public void run() {
        System.out.println("Egg!");
    }

    @Override
    public void start() {
        run();
    }
}