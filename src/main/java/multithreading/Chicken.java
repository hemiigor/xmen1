package multithreading;

public class Chicken extends Thread {

    @Override
    public void run() {
        System.out.println("Chicken ");
    }

    @Override
    public void start() {
        run();
    }
}