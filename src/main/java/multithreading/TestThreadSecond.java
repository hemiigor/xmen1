package multithreading;

public class TestThreadSecond {

    public  static void main(String...args){
        ThreadDemo t1 = new ThreadDemo("Thread 1");
        ThreadDemo t2 = new ThreadDemo("Thread 2");

        t1.start();
        t2.start();
    }
}
