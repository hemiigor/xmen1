package ekkelpackage;

import static firstapp.FirstApp.println;

/**
 * Created by igor on 6/17/18
 *
 * @author Yovkhomishch I.A.
 */
public class BeatBox {

    void testSingleton(){

        Raider.tobattle();
    }
    void testStatic(){
        Murloc murloc = Murloc.tobattle();
    }

    public BeatBox() {
    }

    public static void main(String...args){
       Piano p = new Piano();
       p.f();

       BeatBox beatBox = new BeatBox();
       beatBox.testSingleton();
       beatBox.testStatic();

//       Guitar guitar = new Guitar();
//       println(guitar.b);
//       println(guitar.r);
//       println(guitar.y);

   }

}

class Murloc{
    private Murloc() {
        println("Murloc");
    }
    public  static Murloc tobattle(){
        return new Murloc();
    }
}

class Raider{
    private Raider(){
println("Murloc Raider");
    }
    private static Raider r1 = new Raider();
    public static Raider tobattle(){
        return r1;

    }
}
