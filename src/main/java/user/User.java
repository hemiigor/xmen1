package user;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;


public class User {

   private String username;
   private Integer id;
   private Double price;

    private User(Integer id, Double price, String username) {
        setId(id);
        setUsername(username);
        setPrice(price);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public static void saveUser(User user) {
        ArrayList<Object> userObj = new ArrayList<>();
        userObj.add(user);

        for (int i = 0; i < userObj.size(); i++) {
            System.out.println("User id :" + user.getId());
            System.out.println("User name :" + user.getUsername());
            System.out.println("User price :" + user.getPrice());
            System.out.println(" ");
        }
    }
    public static void main(String... args) {

        String path = "/home/igor/IdeaProjects/xmen1/src/main/resources/file.csv";
        String line;

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null) {

                StringTokenizer stringTokenizer = new StringTokenizer(line, "|");

                while (stringTokenizer.hasMoreElements()) {

                    Integer id = Integer.parseInt(stringTokenizer.nextElement().toString());
                    Double price = Double.parseDouble(stringTokenizer.nextElement().toString());
                    String username = stringTokenizer.nextElement().toString();

                    saveUser(new User(id, price, username));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

