package enums;

public class RequstStatus {

    private final int status;

    private RequstStatus(int aStatus){
        this.status = aStatus;
    }

    public static final RequstStatus STATUS_OPEN = new RequstStatus(0);
    public static final RequstStatus STATUS_STARTED = new RequstStatus(1);
    public static final RequstStatus STATUS_INPROGRESS = new RequstStatus(2);
    public static final RequstStatus STATUS_ONHOLD = new RequstStatus(3);
    public static final RequstStatus STATUS_COMPLETE = new RequstStatus(4);
    public static final RequstStatus STATUS_CLOSED= new RequstStatus(5);

    public void test(){
        RequstStatus requstStatus = RequstStatus.STATUS_CLOSED;
        int status = requstStatus.status;
        System.out.println(status);
    }





}
