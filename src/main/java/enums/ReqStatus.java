package enums;

public class ReqStatus {

    public enum Status{
        STATUS_OPEN,
        STATUS_STARTED,
        STATUS_INPROGRESS,
        STATUS_ONHOLD,
        STATUS_COMLETED,
        STATUS_CLOSE

    }

    public static void main(String args[]){
        for(Status stat :Status.values()){
            System.out.println(stat);
        }
    }
}
