package enums;

public class SatusExampleAbstract {

    public enum Status{

    STATUS_OPEN(0)
            {
                public String description(){
                    return "open";
                }
            },
        STATUS_STARTED(1)
                {

                    public String description() {
                        return "started";
                    }
                },
        STATUS_INPROGRESS(2)
                {

                    public String description() {
                        return "inprogress";
                    }
                },
        STATUS_ONHOLD(3)
                {
                    public String description(){
                        return "onHold";
                    }
                },
        STATUS_COMPLETE(4)
                {

                    public String description() {
                        return "complete";
                    }
                },
        STATUS_CLOSED(5)
                {
                    @Override
                    public String description() {
                        return "closed";
                    }
                };

    int x;
    Status(int x){
        this.x =x;
    }

    public abstract String description();

    public static void data(String value){

        for(Status stat : Status.values()){
            if(stat.name().equalsIgnoreCase(value)){
                System.out.println(stat.description());
            }
        }
    }
}

public static void main(String args[]){
    Status.data("Status_onHold");
}
}
