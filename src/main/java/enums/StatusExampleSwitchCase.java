package enums;

public class StatusExampleSwitchCase {

    public enum Status {
        STATUS_OPEN(0, "open"),
        STATUS_STARTED(1, "started"),
        STATUS_INPROGRESS(2, "inprogress"),
        STATUS_ONHOLD(3, "onhold"),
        STATUS_COMLETED(4, "completed"),
        STATUS_CLOSE(5, "closed");

        private final int status;
        private final String description;

        Status(int aStatus, String desc) {
            this.status = aStatus;
            this.description = desc;
        }

        public int status() {
            return this.status;
        }

        public String description() {
            return this.description;
        }

        private static void checkStatus(Status status){
            switch (status){
                case STATUS_OPEN:
                    System.out.println("this is open status");
                    break;
                case STATUS_STARTED:
                    System.out.println("this is started status");
                    break;
                case STATUS_INPROGRESS:
                    System.out.println("this is inprogress status");
                    break;
                case STATUS_ONHOLD:
                    System.out.println("this is onhold status");
                    break;
                case STATUS_COMLETED:
                    System.out.println("this is status completed");
                    break;
                case STATUS_CLOSE:
                    System.out.println("status is closed");
            }
        }

    }
    public static void main(String args[]){
        Status.checkStatus(Status.STATUS_OPEN);
    }
}
