package enums;

public class SolarSystem {

    public enum Planet {
        PLANET_MERCURY("Mercury", 1) {
            public String description() {
                return Planet.PLANET_MERCURY.name;
            }

            public int inSolar() {
                return Planet.PLANET_MERCURY.position;
            }
        },
        PLANET_VENUS("Venus", 2) {
            public String description() {
                return Planet.PLANET_VENUS.name;
            }

            public int inSolar() {
                return Planet.PLANET_VENUS.position;
            }
        },
        PLANET_EARTH("Earth", 3) {
            @Override
            public String description() {
                return Planet.PLANET_EARTH.name;
            }

            @Override
            public int inSolar() {
                return Planet.PLANET_EARTH.position;
            }
        },
        PLANET_MARS("Mars", 4) {
            public String description() {
                return Planet.PLANET_MARS.name;
            }

            public int inSolar() {
                return Planet.PLANET_MARS.position;
            }
        },
        PLANET_CERES("Ceres", 5) {
            @Override
            public String description() {
                return Planet.PLANET_CERES.name;
            }

            public int inSolar() {
                return Planet.PLANET_CERES.position;
            }
        },

        PLANET_JUPITER("Jupiter", 6) {
            @Override
            public String description() {
                return Planet.PLANET_JUPITER.name;
            }

            public int inSolar() {
                return Planet.PLANET_JUPITER.position;
            }
        },
        PLANET_SATURN("Saturn", 7) {
            @Override
            public String description() {
                return Planet.PLANET_SATURN.name;
            }

            public int inSolar() {
                return Planet.PLANET_SATURN.position;
            }
        },
        PLANET_UARNUS("Uranus", 8) {
            @Override
            public String description() {
                return Planet.PLANET_UARNUS.name;
            }

            public int inSolar() {
                return Planet.PLANET_UARNUS.position;
            }
        },
        PLANET_NEPTUNE("Neptune", 9) {
            @Override
            public String description() {
                return Planet.PLANET_NEPTUNE.name;
            }

            public int inSolar() {
                return Planet.PLANET_NEPTUNE.position;
            }
        };

        public int position;
        public String name;

        public int getPosition() {
            return position;
        }

        public String getName() {
            return name;
        }

        Planet(String name, int number) {
            this.name = name;
            this.position = number;
        }

        public abstract String description();

        public abstract int inSolar();

        public static void data(String value, int srt) {

                for (Planet stat : Planet.values()) {
                    if (stat.getName().equalsIgnoreCase(value)) {
                        System.out.println(stat.description() + srt);
                    }
                }
            }
        }


    }



    

