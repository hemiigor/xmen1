package enums;

public class StatusWithautEnums {

    public static final int STATUS_OPEN = 0;
    public static final int STATUS_STARTED = 1;
    public static final int STATUS_INPROGRES = 2;
    public static final int STATUS_ONHOLD = 3;
    public static final int STATUS_COMPELED = 4;
    public static final int STATUS_CLOSED = 5;

}
