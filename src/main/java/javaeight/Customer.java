package javaeight;

public class Customer {

    private String name;
    private Integer id;
    private String phone;
    private String payCard;


    public Customer(Integer id,String name , String phone, String payCard){
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.payCard = payCard;
    }

    public Customer(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPayCard() {
        return payCard;
    }

    public void setPayCard(String payCard) {
        this.payCard = payCard;
    }

    @Override
    public String toString(){
        return "Customer information ( " + "Name: " + name + " Id: "
                + id + " Phone number: " + phone +" Pay card: " + payCard + " )";
    }


}
