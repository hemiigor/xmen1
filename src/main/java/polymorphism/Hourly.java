package polymorphism;

import polymorphism.Employee;

public class Hourly extends Employee {

    private int hoursWorked;

    /**
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Hourly(String eName, String eAddress, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddress, ePhone, socSecNumber, rate);
        hoursWorked = 0;
    }

    /**
     * Adds the
     * @param moreHours
     */
    public void addHours(int moreHours){
        hoursWorked += moreHours;
    }

    /**
     * Calculate and return the pay for this hourly emloyee.
     * @return
     */
    @Override
    public  double pay(){
        double payment = super.pay() * hoursWorked;
        hoursWorked = 0;
        return payment;
    }

    /**
     * Returns information about this gourly emloyee as a string.
     * @return
     */
    @Override
    public String toString(){
        String result = super.toString();
        result += "\nCurrent hours: " + hoursWorked;
        return  result;
    }

    public static class Executive extends Employee {
    
       private double bonus;
    
        /**
         * Sets up an executive with the specefied information.
         * @param eName
         * @param eAddress
         * @param ePhone
         * @param socSecNumber
         * @param rate
         */
        public Executive(String eName, String eAddress, String ePhone, String socSecNumber, double rate) {
            super(eName, eAddress, ePhone, socSecNumber, rate);
            //bonus has yet to be awarded
            bonus = 0;
        }
    
        /**
         * Awards the specified bonus to this executive.
         * @param execBonus
         */
        public void awardBonus (double execBonus){
            bonus = execBonus;
        }
    
        /**
         * Computes and returns the pay for an executive, which is the regular employee payment plus a one-time bonus.
         * @return
         */
        @Override
        public double pay(){
            double payment = super.pay() + bonus;
            bonus = 0;
            return payment;
        }
    }
}
