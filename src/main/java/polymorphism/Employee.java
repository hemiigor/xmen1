package polymorphism;

public class Employee extends  StaffMember {

    private double payRate;
    private String socialSecurityNumber;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     * @param socSecNumber
     * @param rate
     */
    public Employee(String eName, String eAddress, String ePhone, String socSecNumber, double rate){
        super(eName, eAddress, ePhone);
        socialSecurityNumber = socSecNumber;
        payRate =rate;

    }

    @Override
    public String toString(){
        String result = super.toString();
        result += "\nSocial Security Number: " + socialSecurityNumber;
        return  result;

    }

    @Override
    public double pay() {
        return payRate;
    }
}
