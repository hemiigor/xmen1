package polymorphism;

import interfaces.Bonus;
import interfaces.Hourly;
import interfaces.Rate;

public abstract class Workers {

    private String workerName;
    private String profession;
    private double salary;
    private String payMethod;

    Workers(String workerName,  double salary){

        this.workerName = workerName;
        this.profession = getClass().toString();
        this.salary = salary;

    }



    public abstract double pay();





}
