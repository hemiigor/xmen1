package polymorphism;

abstract public class StaffMember {

    private String name;
    private String address;
    private String phone;

    /**
     *
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    public StaffMember(String eName, String eAddress, String ePhone){
        name = eName;
        address = eAddress;
        phone = ePhone;

    }

    /**
     * Returns a String including the basic employee information.
     * @return
     */
    @Override
    public String toString() {
        String result = "Name: " + name +"\n";
        result += "Address: " + address +"\n";
        result += "Phone: " + phone + "\n";
        return  result;
    }

    /**
     * Deliver classes must define the pay method
     * @return
     */
    public abstract  double pay();
}

