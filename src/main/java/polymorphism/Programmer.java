package polymorphism;

import interfaces.Bonus;
import interfaces.Rate;

public class Programmer extends Workers implements Rate, Bonus {



    private String workerName;
    private String profession;
    private double salary;


    Programmer(String workerName,  double salary) {
        super(workerName,  salary);
        this.workerName = workerName;
        this.salary = salary;
        this.profession = getClass().toString();

    }



    public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public double pay() {
        return  0D;
    }

    @Override
    public void payBonus() {

    }

    @Override
    public void payRate() {

    }

    public static void main(String...args){
        Programmer w = new Programmer("Wasia",1700);

        System.out.println(w.getProfession());
    }
}
