package polymorphism;

public class Volunteer extends StaffMember {

    /**
     * Sets up a volunteer using the specified information.
     * @param eName
     * @param eAddress
     * @param ePhone
     */
    public Volunteer(String eName, String eAddress, String ePhone){
        super(eName,eAddress,ePhone);

    }

    /**
     * Returns a zero pay value for this volunteer.
     * @return
     */
    @Override
    public double pay() {
        return 0.0;
    }
}


