package polymorphism;

import java.util.Date;

public class User {

    private String firstName;
    private String lastName;
    private String loginId;
    private String password;
    private String email;
    private Date registration;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        if (loginId.matches("^[a-zA-Z0-9]*$")) {
            this.loginId = loginId;
        } else {
            System.err.println("Error!");
        }
    }



    public String getPassword() {
        return password;
    }

  /*  public void setPassword(String password) {
        if (password == null || password.length() < 6) {
            System.out.println("Password is to short");
        } else {
            this.password = password;
      */


    public void setPassword(String password) {
        if (password == null || password.length() < 6) {
System.out.println("Password is to short");
        } else {
            this.password = password;
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getRegistration() {
        return registration;
    }

    public void setRegistration(Date registration) {
        this.registration = registration;
    }
}
