package polymorphism;

import interfaces.Fruit;

import java.util.ArrayList;

public class Staff {

    ArrayList<StaffMember> staffList = new ArrayList<>();

    /**
     * Sets up list of staff members.
     */
    public Staff() {
        staffList.add(new Hourly.Executive("Sam","123 Main Line","555-0469", "123-45-6789",2423.07 ));
        staffList.add(new Employee("Carla","456 Off Line","555-0101", "987-65-4321",1246.15 ));
        staffList.add(new Employee("Woody","789 Off Rocker","555-0000", "123-45-6789",1169.23 ));
        staffList.add(new Hourly("Diane","678 Fifth Ave.","555-0690", "123-45-6789",10.55 ));
        staffList.add(new Volunteer("Norm","987 Suds Blvd.","555-8374"));
        staffList.add(new Volunteer("Cliff","321 Duds Lane","555-7282"));

       // ((Executive)staffList.get(0)).awardBonus(500.00);
       // ((Hourly)staffList.get(3)).addHours(40));

        for(StaffMember sm : staffList){
            if (sm instanceof Hourly.Executive)
                ((Hourly.Executive)sm).awardBonus(500);
            else if (sm instanceof Hourly)
                ((Hourly)sm).addHours(40);
        }
    }

    /**
     * Pays all staff members.
     */
    public void payDay() {
        double amount;
        for (int count = 0; count < staffList.size(); count++){
            System.out.println(staffList.get(count));
            amount  = staffList.get(count).pay();

            if(amount == 0.0) {
                System.out.println("Thanks!");
            }else {
                System.out.println("Paid: " + amount);
            }
            System.out.println("--------------------------");
        }
    }
}
