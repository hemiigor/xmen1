package exceptions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class AppZip {

    List<String> fileList = new ArrayList<>();

    private static final String OUTPUT_ZIP_FILE = "/home/igor/IdeaProjects/xmen1/src/main/resources/MyZIP.zip";
    private static final String SOURCE_FOLDER = "/home/igor/IdeaProjects/xmen1/src/main/resources";

    public static void main(String... args) {

        AppZip appZip = new AppZip();
       appZip.generateFileList(new File(SOURCE_FOLDER));
       appZip.zipIt(OUTPUT_ZIP_FILE);

    }

    /**
     * @param node
     */
    public void generateFileList(File node) {

        if (node.isFile()) {
            fileList.add(generateZipEntry(node.getAbsoluteFile().toString()));
        }

        if (node.isDirectory()) {

            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(new File(node, filename));
            }
        }
    }

    /**
     * @param file
     * @return
     */
    private String generateZipEntry(String file) {
        return file.substring(SOURCE_FOLDER.length() + 1, file.length());
    }


    /**
     *
     * @param zipFile
     */
    public void zipIt(String zipFile){

        byte[] buffer = new byte[1024];

        try(FileOutputStream fos = new FileOutputStream(zipFile);
            ZipOutputStream zos = new ZipOutputStream(fos)){

            System.out.println("Output to Zip " + zipFile);

            for(String file : this.fileList){

                ZipEntry ze = new ZipEntry(file);
                zos.putNextEntry(ze);

                try(FileInputStream in = new FileInputStream(SOURCE_FOLDER + File.separator + file)){

                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }

                    System.out.println("File aded " + file);

                    }catch (IOException ex){
                    ex.printStackTrace();
                }
            }
            fos.flush();
            zos.closeEntry();

            System.out.println("Done");
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
