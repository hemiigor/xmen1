package exceptions;

public class CustomerService {

    public static Customer findByName(String name) throws NameNotFoundException{
        if ("".equals(name)){
            throw  new NameNotFoundException(687, "Name is empty!");
        }
        return  new Customer(name);
    }
}
