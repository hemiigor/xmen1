package loops;

public class Matrix {
    Integer id;

    public static Double[][] multiplicar(Double[][] first,Double [][] second){
        int firstColumns = first.length;
        int firstRows = first[0].length;
        int secondColumns = second.length;
        int secondRows = second[0].length;

        if(firstRows != secondColumns){
            throw  new IllegalArgumentException("First Rows :" + firstColumns + "did not match Second Columns " + secondRows+".");

        }
        Double[][] result = new Double[firstRows][secondRows];
        for(int i =0; i <firstColumns; i++){
            for(int j =0; j<secondRows;j++){
                result[i][j] =0.00000;
            }
        }
        for (int i =0; i <firstRows; i++){ //firstRow
            for(int j =0; j < secondColumns; j++){
                for (int k=0; k<result.length; k++){
                    result[i][j] +=first[i][k];
                }
            }
        }
        return result;
    }
}
