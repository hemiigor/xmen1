package loops;

import java.util.Scanner;

/**
 *
 */
public class WhileLoops {

    static {
        System.out.println("Hello if you want to start programm you need to enter 'start'");
        System.out.println("If you want to exit programm enter 'exit'");
    }
    public static void main(String... args) {
        Scanner sc = new Scanner(System.in);
        selectChoice("Hello");

    }
    public static void selectChoice(String str) {


        while (str.compareTo("start") != 0) {
            Scanner enter1 = new Scanner(System.in);
            String choice1 = enter1.next();


            if (choice1.compareTo("start") == 0) {
                System.out.println("Programm starting");
                break;

                } else if(choice1.compareTo("exit") == 0) {
                System.out.println("Goodbye :((");
                System.exit(0);

            }else{
                System.out.println("Wrong command, try again");

            }
        }

            System.out.println("What is the command keyword to exit a loop in Java? ");
            System.out.println("a.quit");
            System.out.println("b.continue");
            System.out.println("c.break");
            System.out.println("d.exit");

            while (str.compareTo("y") != 0) {
                System.out.println("Enter your choice:");

                Scanner enter = new Scanner(System.in);
                String choice = enter.next();

                if (choice.compareTo("c") == 0) {
                    System.out.println("Congratulation!");
                    System.out.println("Exiting");
                    System.exit(0);

                }else if (choice.compareTo("exit") == 0){
                    System.out.println("Exiting");
                    System.exit(0);



                } else
                    System.out.println("Incorrect");
                System.out.println("Again? prees any key to continue:");


            }
        }


}