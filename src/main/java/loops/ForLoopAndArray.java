package loops;

import java.util.Arrays;

public class ForLoopAndArray {
    public static boolean equalityOfTwoArrays(int[] myArray1, int[] myArray2){
        boolean equalOrNot = true;


         if((myArray1 != null && myArray2 != null) && myArray1.length == myArray2.length){





            for(int i =0; i <myArray1.length; i++){

                  if(myArray1[i] != myArray2[i])

                      equalOrNot = false;
            }

        }else
            equalOrNot = false;
        return  equalOrNot;
    }
public static void uniqueArray(int[] myArray){
        int arraySize =myArray.length;

        System.out.println("Origin Array : ");

        for(int i =0; i <arraySize; i++){
            for (int j = i+1; j < arraySize; j++){
                if(myArray[i] == myArray[j]){
                    myArray[j] = myArray[arraySize -1];

                    arraySize--;

                    j--;
                }
            }
        }
        int[] array1 = Arrays.copyOf(myArray, arraySize);
        System.out.println();

        System.out.println("Arrays with unique values : ");

        for(int i =0; i < array1.length;i++)
            System.out.println(array1[i] + "\t");
}
}
