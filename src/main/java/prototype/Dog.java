package prototype;

public class Dog implements Prototype {
    @Override
    public Prototype doClone() {
        return new Dog();
    }
}
