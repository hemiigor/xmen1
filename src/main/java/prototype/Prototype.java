package prototype;

public interface Prototype {
    Prototype doClone();
}
