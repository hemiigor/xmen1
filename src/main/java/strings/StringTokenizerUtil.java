package strings;

import java.util.StringTokenizer;

/**
 * Created by igor 03.03.2018
 */
public class StringTokenizerUtil {
    public static void splitBySpace(String str){
        StringTokenizer st = new StringTokenizer(str);
        System.out.println("--------Split by Space---------");

        while (st.hasMoreElements()){
            System.out.println(st.nextElement());
        }
    }

    public static void splitByComma(String str){
        System.out.println("------Split by Comma ','--------");

        StringTokenizer st2 =new StringTokenizer(str , ",");

        while(st2.hasMoreElements()){
            System.out.println(st2.nextElement());
        }
    }
}
