package factory;

public class Tyronnosaurus extends Animal {
    @Override
    public String makeSound() {
        return "Roar";
    }
}
