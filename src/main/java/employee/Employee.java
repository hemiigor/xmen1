package employee;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class Employee {

   private String nameEmpl;
   private int salary;
   private String position;
   private String subdivision;
   private int age;

    Employee(String nameEmpl, int age, String position, String subdivision, int salary){
        this.nameEmpl = nameEmpl;
        this.age = age;
        this.position = position;
        this.subdivision = subdivision;
        this.salary = salary;
    }

    public String getNameEmpl() {
        return nameEmpl;
    }

    public void setNameEmpl(String nameEmpl) {
        this.nameEmpl = nameEmpl;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getSubdivision() {
        return subdivision;
    }

    public void setSubdivision(String subdivision) {
        this.subdivision = subdivision;
    }

    public  int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static String doSomthing(Employee employee){
        Random random = new Random();
        String str[] ={"work" , "drink a tea", "developed a class", "debug"};
        String work = null;
        int work1 = random.nextInt(str.length);
        work =str[work1];
        return  work;

    }

    public static void writeEmpl(Employee empl){
        try(FileWriter writer = new FileWriter("/home/igor/IdeaProjects/xmen1/src/main/resources/Empl.txt" ,true))
        {
            writer.write(empl.getNameEmpl());
            writer.append('\n');

            writer.write(String.valueOf(empl.getAge()));
            writer.append('\n');

            writer.write(empl.getSubdivision());
            writer.append('\n');

            writer.write(empl.getPosition());
            writer.append('\n');

            writer.write(String.valueOf(empl.getSalary()));
            writer.append('\n');

            writer.write(doSomthing(empl));
            writer.append('\n');
            writer.append("   ");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        }
public static void main(String...args){
    Employee empl = new Employee("Vasia",28,"middle","java",1270);
    Employee empl1 = new Employee("Petia",22,"junior","java",1270);
    Employee empl2 = new Employee("Georg",25,"junior","java",1270);
    Employee empl3 = new Employee("John",29,"junior","C++",1270);
    Employee empl4 = new Employee("Ivan",37,"senior","java",1270);
    Employee empl5 = new Employee("Dmitri",30,"senior","C#",1270);
    Employee empl6= new Employee("Igor",21,"junior","java",1270);
    Employee empl7 = new Employee("Evgenii",26,"middle","java",1270);
    Employee empl8 = new Employee("Gosha",28,"junior","java",1270);
    Employee empl9 = new Employee("Marti",28,"junior","java",1270);

    writeEmpl(empl);
    writeEmpl(empl1);
    writeEmpl(empl2);
    writeEmpl(empl3);
    writeEmpl(empl4);
    writeEmpl(empl5);
    writeEmpl(empl6);
    writeEmpl(empl7);
    writeEmpl(empl8);
    writeEmpl(empl9);
}
}
