package poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class NewExcel {

    public static void main(String...args){

        //Create blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet spreadsheet = workbook.createSheet("Empoyee Info");

        //This data needs to be written (Object[])
        Map<String,Object[]> emplinfo = new TreeMap<>();

        emplinfo.put("1",new Object[]{"EMPL Id", "EMP NAME","DESIGNATION"});
        emplinfo.put("2",new Object[]{"tp01", "Vovan","Technical Manager"});
        emplinfo.put("3",new Object[]{"tp02", "Ivan", "Proof Reader"});
        emplinfo.put("4",new Object[]{"tp03", "Jack","Technical Writer"});
        emplinfo.put("5",new Object[]{"tp04", "Leo","Technical Writer"});
        emplinfo.put("6",new Object[]{"tp05", "Oleg","Technical Writer"});

        //Iterate over data and write to sheet
        Set<String> keyid = emplinfo.keySet();
        //Create row object
        XSSFRow row;

        int rowid = 0;

        for(String key: keyid){
            row = spreadsheet.createRow(rowid++);
                Object[] objects = emplinfo.get(key);
                int cellid = 0;
                for(Object obj : objects){
                    Cell cell =row.createCell(cellid++);
                    cell.setCellValue((String)obj);
            }
        }

        try(FileOutputStream out = new FileOutputStream(new File("Writesheet.xlsx"))){
            workbook.write(out);
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Writesheet.xlsx written successfully");
    }
}
