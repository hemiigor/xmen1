package jsoup;


import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.swing.text.View;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;


public class ThreadController implements Runnable {

    private String url;
    private int counter;
    private Path path = Paths.get(".").toAbsolutePath().getParent();
    private static boolean DEBUG= false;
    private String imageLocation = "/src/main/resources/links/";
    private String outputFolder = "http://websystique.com/";

    public ThreadController(String url){
        this.url=url;
    }


    @Override
    public void run() {

        try{
            File linksFile = new File(path + "/src/main/resources/links/",Thread.currentThread().getName() + ".html");
            try(BufferedWriter linkswriter = new BufferedWriter(new FileWriter(linksFile,true))){

                Connection.Response html = Jsoup.connect(url).execute();
                linkswriter.write(html.body());

                if(DEBUG = true)
                    System.out.println(Thread.currentThread().getName() + " Started");

                counter = TestMain.atomicInteger.incrementAndGet();
                System.out.println(counter + " number of site are checked");
            }
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
