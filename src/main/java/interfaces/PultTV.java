package interfaces;

public class PultTV extends Pult implements ButtonSwitch , SoundRegulator {

    private int volume;

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }



    @Override
    public void switchOn() {
        System.out.println("TV ON");
    }

    @Override
    public void switchOff() {
System.out.println("TV Off");
    }

    @Override
    public void up() {

        volume++;

    }

    @Override
    public void down() {

        volume--;

    }

    @Override
    public void irPort() {

        Integer[] numbers = {1 , 2, 3, 4, 5 , 6, 7, 8, 9};
        for (Integer number : numbers){
            if (number.equals(7)){
                System.out.println("IR port swiched");
                switchOn();
            }
        }

    }
}
