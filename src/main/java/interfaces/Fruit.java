package interfaces;

import polymorphism.StaffMember;

import java.util.Comparator;

public class Fruit implements Comparable<Fruit> {

    private String fruitName;
    private String fruitDesc;
    private int quntity;

    public Fruit(String fruitName, String fruitDesc, int quality) {
        this.fruitDesc = fruitDesc;
        this.fruitName = fruitName;
        this.quntity = quality;

    }

    public String getFruitName() {
        return fruitName;
    }

    public void setFruitName(String fruitName) {
        this.fruitName = fruitName;
    }

    public String getFruitDesc() {
        return fruitDesc;
    }

    public void setFruitDesc(String fruitDesc) {
        this.fruitDesc = fruitDesc;
    }

    public int getQuntity() {
        return quntity;
    }

    public void setQuntity(int quntity) {
        this.quntity = quntity;
    }

    @Override
    public int compareTo(Fruit compareFruit) {

        int compareQuantity = compareFruit.getQuntity();

        return compareQuantity - this.quntity;
    }

    public static Comparator<Fruit> fuitNameComparator = new Comparator<Fruit>() {

        @Override
        public int compare(Fruit fruit1, Fruit fruit2) {
            String fruitName1 = fruit1.getFruitName().toUpperCase();
            String fruitName2 = fruit2.getFruitName().toUpperCase();

            return fruitName1.compareTo(fruitName2);
        }
    };

}