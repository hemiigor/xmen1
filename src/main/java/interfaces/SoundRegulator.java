package interfaces;

public interface SoundRegulator {
    void up();
    void down();
}
