package functional;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Supplier;

public class SuplierTest {

    public  static  void  main(String...args){


        Supplier<User> userFactory =() ->{
            Scanner in = new Scanner(System.in);
            System.out.println("Enter Your Name: ");
            String name = in.nextLine();
            return new User(name);
        };

        User user = userFactory.get();
        User user1 = userFactory.get();

        System.out.println("user 1 name : " + user.getName());
        System.out.println("user 2 name : " + user1.getName());

        Supplier<List<Developer>> test = () -> {
            List<Developer> dev = new ArrayList<>();
            dev.add(new Developer("Data", 4,5));
            return dev;
        };

        test.get().forEach(System.out::println);

        test.get().forEach( x -> {
            System.out.println(x);
        });
    }
}
