package functional;

@FunctionalInterface
public interface SimpleFuncInterface {

    int x =5;

    static void doSomeWork(){
        System.out.println("Do some work ");
    }

    default void doOtherSomWork(){
        System.out.println("Do other some work");
    }

    void dowork();

    String toString();
    boolean equals(Object o);
}
