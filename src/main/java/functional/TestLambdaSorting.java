package functional;

import java.util.ArrayList;
import java.util.List;

public class TestLambdaSorting {


    public static void main(String...args) {

        List<Developer> listDevs = getDevelopers();

        System.out.println("Before sort");
        for (Developer developer : listDevs) {
            System.out.println(developer);
        }

        System.out.println("after sorting");

        listDevs.sort((o1, o2) -> o1.getAge()- o2.getAge());

        listDevs.forEach((developer -> System.out.println(developer)));

    }


        private static List<Developer> getDevelopers(){
            List<Developer> result = new ArrayList<>();
            result.add(new Developer("alex", 70000, 33));
            result.add(new Developer("alvin", 80000, 20));
            result.add(new Developer("jason", 100000, 10));
            result.add(new Developer("iris", 170000, 55));
            return result;
        }
}
