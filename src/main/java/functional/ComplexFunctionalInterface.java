package functional;

@FunctionalInterface
public interface ComplexFunctionalInterface extends SimpleFuncInterface {

    int x =5;

    static void doSomeWork(){
System.out.println("Do some work ");
    }
    @Override
    default void doOtherSomWork(){
        System.out.println("Do other some work");
    }

    @Override
     void dowork();

    @Override
    String toString();

    @Override
    boolean equals(Object o);

}
