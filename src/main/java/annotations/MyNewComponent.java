package annotations;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
//@Target(ElementType.METHOD)
@Inherited
public @interface MyNewComponent {

    String value();

    String name();
    String age();
    String[] newNames();
    String element() default "elem";
}
