package annotations;

@Deprecated
/**
 * @deprecated use MyNewComponent instead
 */
public @interface MyComponent {

    @Deprecated
    String test();
}
