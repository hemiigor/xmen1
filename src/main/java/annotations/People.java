package annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class People implements Serializable, Cloneable {

    private String name;

    private int age;

    public int sum;

    protected int test;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSum() {
        return this.sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getTest() {
        return this.test;
    }

    public void setTest(int test) {
        this.test = test;
    }

    private void test(){

    }
    void test2(){

    }

    @Autowired
    @Deprecated
    protected static  void method(String[] params){

    }

    @Autowired
    @Override
    public String toString(){
        return this.toString();
    }
}
