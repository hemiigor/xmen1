package generics;

public class Pair<KeyType,ValueType> {

    private KeyType key;
    private ValueType value;

    public Pair(KeyType aKey, ValueType aValue){
        key = aKey;
        value = aValue;
    }

    public KeyType getKey() {
        return key;
    }

    public ValueType getValue() {

        return value;
    }

    public void setValue(ValueType aValue) {
        this.value = aValue;
    }

    public static void main(String...args){
        Pair<Integer,String > p = new Pair<>(1,"A");

        System.out.println(p.getKey().getClass().getName());
    }
}
