package generics;

public class Cat extends Animal {


    @Override
    public Cat name(Object o) {
        return this;
    }

    @Override
   public Object getData() {
        return new Cat();
    }
}
