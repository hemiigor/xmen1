package generics;

public abstract class Animal<T> {

    public abstract T name(T t);

    public abstract Object getData();

    public void text(){
        System.out.println("This is Animal!");
    }
}
