package reports;


import static net.sf.dynamicreports.report.builder.DynamicReports.*;

import java.util.Calendar;


import net.sf.dynamicreports.report.builder.style.FontBuilder;
import net.sf.dynamicreports.report.constant.TimePeriod;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;

/**
 * @author Ricardo Mariaca (r.mariaca@dynamicreports.org)
 */
public class DifferenceChartReport {

    public DifferenceChartReport() {
        build();
    }

    private void build() {
        FontBuilder boldFont = stl.fontArialBold().setFontSize(12);

        try {
            report()
                    .setTemplate(Templates.reportTemplate)
                    .title(Templates.createTitleComponent("DifferenceChart"))
                    .summary(
                            cht.differenceChart()
                                    .setTitle("Difference chart")
                                    .setTitleFont(boldFont)
                                    .setTimePeriod(field("date", type.dateType()))
                                    .setTimePeriodType(TimePeriod.DAY)
                                    .series(
                                            cht.serie(field("value1", type.doubleType())).setLabel("Value1"),
                                            cht.serie(field("value2", type.doubleType())).setLabel("Value2"))
                                    .setTimeAxisFormat(
                                            cht.axisFormat().setLabel("Date")))
                    .pageFooter(Templates.footerComponent)
                    .setDataSource(createDataSource())
                    .show();
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private JRDataSource createDataSource() {
        DRDataSource dataSource = new DRDataSource("date", "value1", "value2");
        double value1 = 0;
        double value2 = 0;
        Calendar c = Calendar.getInstance();
        for (int i = 0; i < 300; i++) {
            c.add(Calendar.DAY_OF_MONTH, -1);
            value1 += Math.random() - 0.5;
            value2 += Math.random() - 0.5;
            dataSource.add(c.getTime(), value1, value2);
        }
        return dataSource;
    }

    public static void main(String[] args) {
        new DifferenceChartReport();
    }
}

