package maps;

import java.util.*;

public class TreeMapExample {

    public static void main(String... args) {
        Map<String, String> treeMap = new TreeMap<>();
        treeMap.put("Bruce", "Willis");
        treeMap.put("Arnold", "Schwarz");
        treeMap.put("Jackie", "Chan");
        treeMap.put("Sylvester", "Stalone");
        treeMap.put("Chuck", "Noris");

        for (Map.Entry tree : treeMap.entrySet()) {
            System.out.println(tree.getKey() + "" + tree.getValue());
        }

        TreeMap<String, Double> tradeMarkMap = new TreeMap<>();

        tradeMarkMap.put("Zara", 8284.37);
        tradeMarkMap.put("Bershka ", 354.11);
        tradeMarkMap.put("Cropp ", 2363.00);
        tradeMarkMap.put("Stradivary ", 78.12);
        tradeMarkMap.put("Pull & Bear", -25.34);

        tradeMarkMap.forEach((key, value) -> {
            System.out.println(key + " = " + value);
            System.out.println();
        });


        System.out.println("===========================");

        Set set = tradeMarkMap.entrySet();

        Iterator i = set.iterator();

        while (i.hasNext()) {
            Map.Entry entryMap = (Map.Entry) i.next();
            System.out.println(entryMap.getKey() + ": ");
            System.out.println(entryMap.getValue());
        }
        System.out.println();


        double balance = tradeMarkMap.get("Zara");
        tradeMarkMap.put("Zara", balance + 1000);
        System.out.println("Zara s new Balance " + tradeMarkMap.get("Zara"));


        Map<User, String> userMap = new TreeMap<>(new UserSalaryComparator());

        userMap.put(new User("Nil", "Tompson", 9826), "My Name 1");
        userMap.put(new User("Deryl", "Logan", 45986), "My Name 2");
        userMap.put(new User("Andriy", "Shevchenko", 96), "My Name 3");
        userMap.put(new User("Pedro", "Gonzales", 9), "My Name 4");

        System.out.println(userMap.toString());

        Set keySet = userMap.entrySet();
        for (Object user : keySet) {
            System.out.println(user);
        }


       Map<Car, String> userCar = new TreeMap<>(new CarComparatorTM().thenComparing(new CarComparatorId().thenComparing(new CarComparatorYear().thenComparing( new CarComparatorMod()))));




        userCar.put(new Car(123, "Audi", "A6", 2006), "9700");
        userCar.put(new Car(129, "Skoda", "SuperB", 2016), "13000");
        userCar.put(new Car(178, "Peugeot", "508", 2014), "11300");
        userCar.put(new Car(1, "Mitsubishi", "Pajero Sport", 2017), "22000");

        System.out.println(userCar.toString());

        Set keySet1 = userCar.entrySet();
        for (Object user : keySet1) {
            System.out.println(user);

        }

    }
}
