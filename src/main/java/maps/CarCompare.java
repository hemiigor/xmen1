package maps;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class CarCompare {

    public static void main(String... args) {
        Map<Car, String> userCar = new TreeMap<>(new CarComparatorTM().thenComparing(new CarComparatorId().thenComparing(new CarComparatorYear().thenComparing(new CarComparatorMod()))));


        userCar.put(new Car(123, "Audi", "A6", 2006), "9700");
        userCar.put(new Car(129, "Skoda", "SuperB", 2016), "13000");
        userCar.put(new Car(178, "Peugeot", "508", 2014), "11300");
        userCar.put(new Car(1, "Mitsubishi", "Pajero Sport", 2017), "22000");

        System.out.println(userCar.toString());
        System.out.println();

        Set keySet1 = userCar.entrySet();
        for (Object user : keySet1) {
            System.out.println(user);

        }
    }
}
