package maps;

import javax.management.ObjectName;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class CarComparatorTM implements Comparator<Car> {


    @Override
    public int compare(Car o1, Car o2) {

        return o1.getTrademark().compareTo(o2.getTrademark());

    }
}

