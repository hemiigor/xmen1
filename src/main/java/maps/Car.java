package maps;

public class Car {

    private int id;
    private String trademark;
    private String model;
    private int year;

    public Car(int id, String trademark, String  model, int year){
        this.id = id;
        this.trademark = trademark;
        this.model = model;
        this.year = year;

    }

    public int getId() {
        return id;
    }

    public String getTrademark() {
        return trademark;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString(){
        return "Trademark : " + getTrademark() + ". Model: " + getModel() + ". Id: " + getId() + ". Year of esteblishment " + getYear();
    }
}
