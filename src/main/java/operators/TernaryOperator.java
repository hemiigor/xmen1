package operators;

public class TernaryOperator {
    private final Integer FIRST = 5;

    public Integer getFIRST() {
        return FIRST;
    }

    public Integer getSECOND() {
        return SECOND;
    }

    /**

     *
     */

    private final Integer SECOND = 10;

    /**
     * Get Minimal Value
     * @param i
     * @param j
     * @return
     */
    public static int getMinValue(int i, int j){
        return (i < j) ? i:j;

    }

    /**
     * Get Absolut Value
     * @param i
     * @return
     */
    public static int getAbsoluteValue(int i){
        return i < 0 ? -i : i;
    }

    /**
     * invert boolean
     * @param b
     * @return
     */
    public static boolean invertBoolean(boolean b){
        return b ? false : true;
    }

    /**
     * Check if String contains "A"
     * @param str
     */
    public static void containsA(String str){
        String data = str.contains("A")? "Str contains 'A'" : "Str doesn*t contains 'A'";
        System.out.println(data);
    }

    /**
     *
     * @param i
     * @param ternaryOperator
     */
    public static void ternaryMethod(Integer i, TernaryOperator ternaryOperator ){
        System.out.println((i.equals(ternaryOperator.getFIRST()))
                ? "i =5" : ((i.equals(ternaryOperator.getSECOND())) ? "i=10" : "i is not equals to 5 or 10"));

    }
}

